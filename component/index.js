import Page1 from "./page1";
import Page2 from "./page2";
import Page3 from "./page3";
import Page3_1 from "./page3_1"
import Page4 from "./page4";
import Page5 from "./page5";
import Page6 from "./page6";

export { Page1, Page2, Page3, Page3_1, Page4, Page5, Page6 };
