import React from "react";
import { Row, Col } from "react-bootstrap";

function page1() {
  return (
    <div className="page1 d-flex flex-column align-items-center justify-content-center ">
      <Row
        className="h-70 d-flex flex-column align-items-center justify-content-center"
        style={{ width: "60%", background: "none", zIndex: "2" }}
      >
        <Col xs={12} s={12} className="d-flex justify-content-center">
          <p className="fontTop" style={{ fontSize: "56px" }}>
            LEARNING SET
          </p>
        </Col>
        <Col
          s={12}
          xs={12}
          className="d-flex align-items-center justify-content-center"
          style={{ background: "none", marginTop: "" }}
        >
          <img
            src="gearBluee.png"
            width="80px"
            height="80px"
            className="ms-3"
          />
          <p className="fontTop" style={{ fontSize: "56px" }}>
            F
          </p>
        </Col>
        <Col
          s={12}
          xs={12}
          className="d-flex align-items-center justify-content-center"
          style={{ marginTop: "" }}
        >
          <p className="fontTop" style={{ fontSize: "48.8px" }}>
            REDUCTION GEAR SYSTEM
          </p>
        </Col>
      </Row>
      <div
        className="mt-5 w-100 h-30 d-flex justify-content-center align-items-center"
        style={{ zIndex: "2" }}
      >
        <div
          className="d-flex p-5 h-100 align-items-center"
          style={{ background: "#CAE8FF", cursor: "pointer", fontSize: "36px", borderRadius: "20px" }}
          onClick={() => {
            window.location.href = "https://drive.google.com/file/d/15Ywh-ceegk_TUF8T1rwStDx-4Cnwk4_H/preview"
          }}
        >
          <p>ใบความรู้&nbsp;</p>
          <a style={{ color: "#5271FF" }}>(คลิก)</a>
        </div>
      </div>
    </div>
  );
}

export default page1;
