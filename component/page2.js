import React, { useEffect } from "react";
import { Row, Col } from "react-bootstrap";
// import knowledge from "../public/knowledge.pdf"

function page2() {
  return (
    <div className="page d-flex flex-column justify-content-start align-items-center">
      <div
        className="d-flex justify-content-center"
        style={{ zIndex: "2", width: "95%" }}
      >
        <Row
          className="p-2 rounded"
          style={{ width: "100%", background: "#D4EDF4" }}
        >

          <Col xs={12} className="mb-5" style={{ fontSize: "26px" }}>
            <p style={{ fontSize: "36px" }}>
              <b>เคสตัวอย่าง</b>
            </p>
            <p>
              นำมอเตอร์ไฟฟ้า ไปใช้ยกวัตถุที่มีมวล 1500 g ผ่านเพลาส่งที่มีรัศมี
              0.005 m จงคำนวณหาแรงบิดมอเตอร์และแรงบิดเชิงกล
            </p>
            <p>
              (กำหนดให้มอเตอร์ไฟฟ้า มีความเร็วรอบเท่ากัย 200 RPM
              และค่าแรงบิดเท่ากับ 0.05 N.M.)
            </p>
          </Col>
          <Col
            xs={6}
            style={{
              fontSize: "20px",
              border: "solid",
              borderWidth: "3px",
              borderTopWidth: "0px",
              borderLeftWidth: "0px",
              borderBottomWidth: "0px",
              borderStyle: "dotted",
              borderColor: "#F46530",
            }}
          >
            <p className="w-100 d-flex justify-content-start">
              <b>แรงต้านของวัตถุหาได้จาก</b> &nbsp;มวล(Kg)xค่าแรงโนมถ่วง 9.81
              m/s^2
            </p>
            <p style={{ fontSize: "20px" }}>
              <b>หาแรงบิดของวัตถุ</b>
            </p>
            <Row>
              <Col xs={4}>
                <p className="w-100 d-flex justify-content-end">แรงบิด(T)</p>
              </Col>
              <Col xs={1}>
                <p className="w-100 d-flex justify-content-center"> =</p>
              </Col>
              <Col xs={7}>
                <p className="w-100 d-flex justify-content-start">
                  แรงต้าน(F) x รัศมีของเพลา (r)
                </p>
              </Col>
              <Col xs={4}></Col>
              <Col xs={1}>
                <p className="w-100 d-flex justify-content-center">=</p>
              </Col>
              <Col xs={7}>
                <p className="w-100 d-flex justify-content-start">
                  1.5kg x 9.81 x 0.005
                </p>
              </Col>
              <Col xs={4}></Col>
              <Col xs={1}>
                <p className="w-100 d-flex justify-content-center">=</p>
              </Col>
              <Col xs={7}>
                <p className="w-100 d-flex justify-content-start">0.074 N.m</p>
              </Col>
            </Row>
            <p className="w-100 d-flex justify-content-center mt-4">
              จากโจทย์แรงบิดของมอเตอร์มีค่า 0.05 N.M.
            </p>
            <p className="w-100 d-flex justify-content-center">
              ทำให้ไม่สามารถยกวัตถุที่มีแรงบิด 0.074 N.M. ได้
            </p>
            <p className="w-100 d-flex justify-content-center mb-4">
              การที่จะยกวัตถุขึ้นได้จะต้องทำให้มอเตอร์มีค่าแรงบิดที่สูงกว่าค่าแรงบิดของวัตถุ
            </p>
            {/* <p style={{ fontSize: "20px" }}>
              <b>หาอัตราทดเกียร์ขั้นต่ำ</b>
            </p> */}
            {/* <Row>
              <Col xs={4}>
                <p className="w-100 d-flex justify-content-end">
                  อัตตราทดเกียร์
                </p>
              </Col>
              <Col xs={1}>
                <p className="w-100 d-flex justify-content-center"> =</p>
              </Col>
              <Col xs={7}>
                <p className="w-100 d-flex justify-content-start">
                  แรงบิดวัตถุ (To) / แรงบิดมอเตอร์ (Tm)
                </p>
              </Col>
              <Col xs={4}></Col>
              <Col xs={1}>
                <p className="w-100 d-flex justify-content-center">=</p>
              </Col>
              <Col xs={7}>
                <p className="w-100 d-flex justify-content-start">
                  0.074 / 0.05
                </p>
              </Col>
              <Col xs={4}></Col>
              <Col xs={1}>
                <p className="w-100 d-flex justify-content-center">=</p>
              </Col>
              <Col xs={7}>
                <p className="w-100 d-flex justify-content-start">1.48</p>
              </Col>
            </Row> */}
            <div className="d-flex justify-content-space-around w-100">
              <p>เลือก</p>
              <p>เฟืองที่ขับ = 10 ซี่</p>
              <p>เฟืองตาม = 30 ซี่</p>
            </div>
            <p>อัตตราทดเกียร์ = จำนวนเฟืองขับ(Z2) / จำนวนเฟืองตาม (Z1)</p>
            <p>อัตตราทดเกียร์ = 30 / 10 = 3</p>
          </Col>
          {/* <Col xs={1}>
            <div
              className="h-100"
              style={{
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderBottomWidth: "0px",
                borderStyle: "dotted",
                borderColor: "#F46530",
              }}
            ></div>
          </Col> */}
          <Col xs={6} style={{ fontSize: "20px" }}>
            <p className="">
              <b>ควาเร็วรอบเมื่อต่ออัตราทดเกียร์ </b>
            </p>
            {/* <p className="ms-3 mt-2">
              อัตราทดเกียร์ = ความเร็วรอบมอเตอร์ / ความเร็วรอบผ่านอัตราทดเกียร์
            </p> */}
            <p className="ms-3 mt-2">
              ความเร็วรอบเมื่อต่อผ่านอัตราทดเกียร์ = ความเร็วรอบมอเตอร์ /
              อัตราทดเกียร์
            </p>
            <p className="ms-3 mt-2">
              ความเร็วรอบผ่านอัตราทดเกียร์ = 200 / 3 = 66.67 RPM
            </p>
            <p className="mt-3">
              <b>แรงบิดมอเตอร์เมื่อต่ออัตราทดเกียร์ </b>
            </p>
            {/* <p className="ms-3 mt-2">
              อัตราทดเกียร์ = แรงบิดเมื่อต่ออัตราทดเกียร์ * แรงบิดมอเตอร์
            </p> */}
            <p className="ms-3 mt-2">
              แรงบิดมอเตอร์เมื่อต่ออัตราทดเกียร์ = แรงบิดมอเตอร์ x อัตตราทดเกียร์
            </p>
            {/* <p className="ms-3 mt-2">
              แรงบิดเมื่อต่ออัตราทดเกียร์ = 3 x 200 = 600{" "}
            </p> */}
            <p className="ms-3 mt-2">
              แรงบิดมอเตอร์เมื่อต่ออัตราทดเกียร์ = 0.05 x 3 = 0.15 N.M.
            </p>
            <p className="mt-3">
              <b>กำลังไฟฟ้ามอเตอร์เมื่อต่ออัตราทดเกียร์ </b>
            </p>
            <p className="ms-3 mt-2 w-100">
              กำลังไฟฟ้า = แรงบิดมอเตอร์เมื่อต่ออัตราทดเกียร์ x
              ควาเร็วรอบเมื่อต่ออัตราทดเกียร์ / 9.55
            </p>
            <p className="mt-3">กำลังไฟฟ้า = 0.15 x 66.67 /9.55 </p>
            <p className="mt-3">กำลังไฟฟ้า = 1.04 W</p>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default page2;
