import React, { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";

const ex = {
  one: {
    ex: "นำมอเตอร์ไฟฟ้า ไปใช้ยกวัตถุที่มีมวล 2000 g ผ่านเพลาส่งที่มีรัศมี 0.005 m จงคำนวณหาแรงบิดมอเตอร์และแรงบิดเชิงกล",
    grum: 2000,
    ras: 0.098,
  },
  two: {
    ex: "นำมอเตอร์ไฟฟ้า ไปใช้ยกวัตถุที่มีมวล 2500 g ผ่านเพลาส่งที่มีรัศมี 0.005 m คำนวณหาแรงบิดมอเตอร์และแรงบิดเชิงกล",
    grum: 2500,
    ras: 0.123,
  },
  three: {
    ex: "นำมอเตอร์ไฟฟ้า ไปใช้ยกวัตถุที่มีมวล 3000 g ผ่านเพลาส่งที่มีรัศมี 0.005 m คำนวณหาแรงบิดมอเตอร์และแรงบิดเชิงกล",
    grum: 3000,
    ras: 0.147,
  },
  four: {
    ex: "นำมอเตอร์ไฟฟ้า ไปใช้ยกวัตถุที่มีมวล 3500 g ผ่านเพลาส่งที่มีรัศมี 0.005 m คำนวณหาแรงบิดมอเตอร์และแรงบิดเชิงกล",
    grum: 3500,
    ras: 0.172,
  },
  five: {
    ex: "นำมอเตอร์ไฟฟ้า ไปใช้ยกวัตถุที่มีมวล 4000 g ผ่านเพลาส่งที่มีรัศมี 0.005 m คำนวณหาแรงบิดมอเตอร์และแรงบิดเชิงกล",
    grum: 4000,
    ras: 0.196,
  },
  six: {
    ex: "นำมอเตอร์ไฟฟ้า ไปใช้ยกวัตถุที่มีมวล 4500 g ผ่านเพลาส่งที่มีรัศมี 0.005 m คำนวณหาแรงบิดมอเตอร์และแรงบิดเชิงกล",
    grum: 4500,
    ras: 0.221,
  },
  seven: {
    ex: "นำมอเตอร์ไฟฟ้า ไปใช้ยกวัตถุที่มีมวล 5000 g ผ่านเพลาส่งที่มีรัศมี 0.005 m คำนวณหาแรงบิดมอเตอร์และแรงบิดเชิงกล",
    grum: 5000,
    ras: 0.245,
  },
};

function page3({ handleChangeGrum = () => {}, handleChangeRas = () => {} }) {
  const [key, setKey] = useState("one");

  useEffect(() => {
    handleChangeGrum(ex[key].grum);
    handleChangeRas(ex[key].ras);
  }, [key]);

  return (
    <div className="page d-flex flex-column justify-content-start align-items-center">
      <Row className="w-75 h-75 d-flex justify-content-center">
        <Col xs={3}>
          <div
            className="h-100 d-flex flex-column align-items-center p-3 rounded"
            style={{ background: "#82AAE3" }}
          >
            <p
              className="p-3 rounded"
              style={{
                fontSize: "36px",
                background: "#12229D",
                color: "white",
              }}
            >
              พิกัดมอเตอร์
            </p>
            <div className="h-75 d-flex flex-column align-items-center justify-content-center">
              <img src="gearBoll.png" className="mt-5" width={"60%"} />
              <p
                className="p-3 mt-5 rounded"
                style={{
                  fontSize: "26px",
                  background: "white",
                  textAlign: "center",
                }}
              >
                แรงดันไฟฟ้า&nbsp;&nbsp;&nbsp;6V
              </p>
              <p
                className="p-2 mt-3 rounded"
                style={{
                  fontSize: "26px",
                  background: "white",
                  textAlign: "center",
                }}
              >
                กระแสไฟฟ้าเมื่อไม่มีโหลด&nbsp;&nbsp;&nbsp;0.17A
              </p>
              <p
                className="p-2 mt-3 rounded"
                style={{
                  fontSize: "26px",
                  background: "white",
                  textAlign: "center",
                }}
              >
                กำลังไฟฟ้า 1 W
              </p>
              <p
                className="p-2 mt-3 rounded"
                style={{
                  fontSize: "26px",
                  background: "white",
                  textAlign: "center",
                }}
              >
                ความเร็วรอบ 200 RPM
              </p>
            </div>
          </div>
        </Col>
        <Col xs={9}>
          <div
            className="w-100 h-100 d-flex flex-column align-items-center"
            style={{ background: "#12229D" }}
          >
            <div
              className="w-100 p-3 rounded d-flex"
              style={{ background: "#CAE8FF" }}
            >
              <p
                className="w-25 p-2 d-flex justify-content-center align-items-center rounded"
                style={{
                  fontSize: "36px",
                  background: "#12229D",
                  color: "white",
                }}
              >
                โจทย์
              </p>
              <div className="w-75 d-flex justify-content-around">
                <div className="d-flex flex-column align-items-center">
                  <p
                    onClick={() => {
                      setKey("one");
                    }}
                    className="p-3 d-flex justify-content-center align-items-center rounded-circle"
                    style={{
                      fontSize: "26px",
                      background: "#12229D",
                      color: "white",
                      width: "50px",
                      height: "50px",
                      cursor: "pointer",
                    }}
                  >
                    1
                  </p>
                  {key == "one" ? (
                    <img
                      src="arraow.png"
                      className="mt-2"
                      width={"26px"}
                      height={"26px"}
                    />
                  ) : null}
                </div>
                <div className="d-flex flex-column align-items-center">
                  <p
                    onClick={() => {
                      setKey("two");
                    }}
                    className="p-3 d-flex justify-content-center align-items-center rounded-circle"
                    style={{
                      fontSize: "26px",
                      background: "#12229D",
                      color: "white",
                      width: "50px",
                      height: "50px",
                      cursor: "pointer",
                    }}
                  >
                    2
                  </p>
                  {key == "two" ? (
                    <img
                      src="arraow.png"
                      className="mt-2"
                      width={"26px"}
                      height={"26px"}
                    />
                  ) : null}
                </div>
                <div className="d-flex flex-column align-items-center">
                  <p
                    onClick={() => {
                      setKey("three");
                    }}
                    className="p-3 d-flex justify-content-center align-items-center rounded-circle"
                    style={{
                      fontSize: "26px",
                      background: "#12229D",
                      color: "white",
                      width: "50px",
                      height: "50px",
                      cursor: "pointer",
                    }}
                  >
                    3
                  </p>
                  {key == "three" ? (
                    <img
                      src="arraow.png"
                      className="mt-2"
                      width={"26px"}
                      height={"26px"}
                    />
                  ) : null}
                </div>
                <div className="d-flex flex-column align-items-center">
                  <p
                    onClick={() => {
                      setKey("four");
                    }}
                    className="p-3 d-flex justify-content-center align-items-center rounded-circle"
                    style={{
                      fontSize: "26px",
                      background: "#12229D",
                      color: "white",
                      width: "50px",
                      height: "50px",
                      cursor: "pointer",
                    }}
                  >
                    4
                  </p>
                  {key == "four" ? (
                    <img
                      src="arraow.png"
                      className="mt-2"
                      width={"26px"}
                      height={"26px"}
                    />
                  ) : null}
                </div>
                <div className="d-flex flex-column align-items-center">
                  <p
                    onClick={() => {
                      setKey("five");
                    }}
                    className="p-3 d-flex justify-content-center align-items-center rounded-circle"
                    style={{
                      fontSize: "26px",
                      background: "#12229D",
                      color: "white",
                      width: "50px",
                      height: "50px",
                      cursor: "pointer",
                    }}
                  >
                    5
                  </p>
                  {key == "five" ? (
                    <img
                      src="arraow.png"
                      className="mt-2"
                      width={"26px"}
                      height={"26px"}
                    />
                  ) : null}
                </div>
                <div className="d-flex flex-column align-items-center">
                  <p
                    onClick={() => {
                      setKey("six");
                    }}
                    className="p-3 d-flex justify-content-center align-items-center rounded-circle"
                    style={{
                      fontSize: "26px",
                      background: "#12229D",
                      color: "white",
                      width: "50px",
                      height: "50px",
                      cursor: "pointer",
                    }}
                  >
                    6
                  </p>
                  {key == "six" ? (
                    <img
                      src="arraow.png"
                      className="mt-2"
                      width={"26px"}
                      height={"26px"}
                    />
                  ) : null}
                </div>
                <div className="d-flex flex-column align-items-center">
                  <p
                    onClick={() => {
                      setKey("seven");
                    }}
                    className="p-3 d-flex justify-content-center align-items-center rounded-circle"
                    style={{
                      fontSize: "26px",
                      background: "#12229D",
                      color: "white",
                      width: "50px",
                      height: "50px",
                      cursor: "pointer",
                    }}
                  >
                    7
                  </p>
                  {key == "seven" ? (
                    <img
                      src="arraow.png"
                      className="mt-2"
                      width={"26px"}
                      height={"26px"}
                    />
                  ) : null}
                </div>
              </div>
            </div>
            <div
              className="h-75 my-5 rounded d-flex flex-column align-items-center"
              style={{ background: "#CAE8FF", width: "90%", fontSize: "26px" }}
            >
              <p className="p-3 w-100" style={{ textAlign: "center" }}>
                <b>{ex[key].ex}</b>
              </p>
              <p className="w-100 p-3">
                <b>สูตรคำนวณหาแรงบิดมอเตอร์</b>
              </p>
              <p className="w-100 p-3 ml-3">
                หาได้จากสูตร Tm = (9.55 x แรงดันไฟฟ้า x กระแสไฟฟ้า) /
                ความเร็วรอบ{" "}
              </p>
              <p className="w-100 p-3">
                <b>สูตรคำนวณหาแรงบิดเชิงกล</b>
              </p>
              <p className="w-100 p-3 ml-3">
                แรงบิดวัตถุ(To) = แรงต้าน(F) x รัศมีของเพลา (r)
              </p>
              <p className="w-100 p-3 ml-3">แรงต้าน (F) = m x g</p>
              <p className="w-100 p-3 ml-3">
                m ค่าน้ำหนักหน่วย kg , g = 9.81 m/s^2
              </p>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default page3;
