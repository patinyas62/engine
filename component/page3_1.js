import React, { useEffect } from "react";
import { Row, Col } from "react-bootstrap";
// import knowledge from "../public/knowledge.pdf"

function page2({ras}) {
  return (
    <div className="page d-flex flex-column justify-content-start align-items-center">
      <div
        className="mt-5 h-75 d-flex justify-content-center"
        style={{ zIndex: "2", width: "90%" }}
      >
        <Row
          className="p-5 rounded"
          style={{ width: "100%", background: "white", border: "solid",borderWidth: "20px", borderColor: "#CAE8FF" }}
        >
          <Col xs={5} className="d-flex justify-content-center align-items-center">
            <img src="blueleft.png" width={"70%"} />
          </Col>
          <Col xs={2} className="d-flex justify-content-center align-items-end">
            <img src="think.png" width={"50%"} />
          </Col>
          <Col xs={5} className="d-flex justify-content-center align-items-center">
            <img src="blueright.png" width={"40%"} />
          </Col>
          <Col xs={5} className="d-flex justify-content-center align-items-center">
            <div>
                <p className="p-3" style={{fontSize: "22px", background: "#12229D", color: "white"}}><b>แรงบิดมอเตอร์เริ่มต้น 0.05 N.m</b></p>
            </div>
          </Col>
          <Col xs={2} className="d-flex justify-content-center align-items-center">
            <div>
                <p className="p-3" style={{fontSize: "22px", background: "none", color: "white"}}><b>{ras < 0.05 ? <img src="sameblue.png" width={"100%"} /> : <img src="nosameblue.png" width={"100%"} />}</b></p>
            </div>
          </Col>
          <Col xs={5} className="d-flex justify-content-center align-items-center">
            <div className="p-3 d-flex" style={{fontSize: "22px", background: "#12229D", color: "white"}}>
                <p ><b>แรงบิดเชิงกล</b></p>
                <p><b>&nbsp;{ras}</b></p>
                <p ><b>&nbsp;N.m.</b></p>
            </div>
          </Col>
          <p className="w-100 d-flex justify-content-center mt-2" style={{color: "black",fontSize: "26px"}}><b>&nbsp;*หากค่าแรงบิดของมอเตอร์มีค่ามากกว่าแรงบิดเชิงกล จะทำให้มอเตอร์มีความเหมาะสมต่อการใช้งาน</b></p>
        </Row>
      </div>
    </div>
  );
}

export default page2;
