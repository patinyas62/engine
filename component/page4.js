import React, { useState, useEffect } from "react";
import { Row, Col, Form } from "react-bootstrap";
import Dropdown from "react-bootstrap/Dropdown";
import InputGroup from "react-bootstrap/InputGroup";
import cal10 from "../utiles/cal_10";
import cal20 from "../utiles/cal_20";
import cal30 from "../utiles/cal_30";
import cal40 from "../utiles/cal_40";
import cal50 from "../utiles/cal_50";
// import Form from "react-bootstrap/Form";

const mass = [
  {
    value: 500,
    label: "500",
  },
  {
    value: 1000,
    label: "1000",
  },
  {
    value: 1500,
    label: "1500",
  },
  {
    value: 2000,
    label: "2000",
  },
  {
    value: 2500,
    label: "2500",
  },
  {
    value: 3000,
    label: "3000",
  },
  {
    value: 3500,
    label: "3500",
  },
  {
    value: 4000,
    label: "4000",
  },
  {
    value: 4500,
    label: "4500",
  },
  {
    value: 5000,
    label: "5000",
  },
];

const gearRatio = [
  {
    value: 0,
    label: "0",
  },
  {
    value: 10,
    label: "10",
  },
  {
    value: 30,
    label: "30",
  },
];

const gearRatioTwo = [
  {
    value: 0,
    label: "0",
  },
  {
    value: 10,
    label: "10",
  },
];

const gearRatioThree = [
  {
    value: 0,
    label: "0",
  },
  {
    value: 30,
    label: "30",
  },
  {
    value: 40,
    label: "40",
  },
  {
    value: 50,
    label: "50",
  },
];

function page4({ handleChangeData = () => {}, grum, ras }) {
  const [listByOne, setListByOne] = useState(false);
  const [listByTwo, setListByTwo] = useState(false);

  const [valueOne, setValueOne] = useState(0);
  const [valueTwo, setValueTwo] = useState(0);
  const [valueThree, setValueThree] = useState(0);
  const [valueFour, setValueFour] = useState(0);

  const [rating, setRating] = useState(0);
  const [rpm, setRpm] = useState(0);
  const [powerm, setPower] = useState(0);

  const [checkValue, setCheckValue] = useState(false);

  const [show, setShow] = useState(0);

  const handleSend = () => {
    if (valueOne == 10) {
      setShow(
        cal10(
          [
            parseInt(valueTwo),
            parseInt(valueThree),
            parseInt(valueFour),
            parseInt(valueOne),
          ],
          parseInt(grum)
        )
      );
    } else if (valueOne == 20) {
      setShow(
        cal20(
          [
            parseInt(valueTwo),
            parseInt(valueThree),
            parseInt(valueFour),
            parseInt(valueOne),
          ],
          parseInt(grum)
        )
      );
    } else if (valueOne == 30) {
      setShow(
        cal30(
          [
            parseInt(valueTwo),
            parseInt(valueThree),
            parseInt(valueFour),
            parseInt(valueOne),
          ],
          parseInt(grum)
        )
      );
    } else if (valueOne == 40) {
      setShow(
        cal40(
          [
            parseInt(valueTwo),
            parseInt(valueThree),
            parseInt(valueFour),
            parseInt(valueOne),
          ],
          parseInt(grum)
        )
      );
    } else if (valueOne == 50) {
      setShow(
        cal50(
          [
            parseInt(valueTwo),
            parseInt(valueThree),
            parseInt(valueFour),
            parseInt(valueOne),
          ],
          parseInt(grum)
        )
      );
    }
  };

  const handleSendTwo = (one, two, three, four) => {
    if (one == 10) {
      setShow(
        cal10(
          [parseInt(two), parseInt(three), parseInt(four), parseInt(one)],
          parseInt(grum)
        )
      );
    } else if (one == 20) {
      setShow(
        cal20(
          [parseInt(two), parseInt(three), parseInt(four), parseInt(one)],
          parseInt(grum)
        )
      );
    } else if (one == 30) {
      setShow(
        cal30(
          [parseInt(two), parseInt(three), parseInt(four), parseInt(one)],
          parseInt(grum)
        )
      );
    } else if (one == 40) {
      setShow(
        cal40(
          [parseInt(two), parseInt(three), parseInt(four), parseInt(one)],
          parseInt(grum)
        )
      );
    } else if (one == 50) {
      setShow(
        cal50(
          [parseInt(two), parseInt(three), parseInt(four), parseInt(one)],
          parseInt(grum)
        )
      );
    }
  };

  return (
    <div
      className="page2 d-flex flex-column justify-content-start align-items-center"
      style={{ zIndex: "2" }}
    >
      <div
        className="h-75 d-flex justify-content-center"
        style={{ zIndex: "2", width: "90%" }}
      >
        <Row
          className="p-2 rounded d-flex justify-content-center"
          style={{
            width: "100%",
            background: "white",
            border: "solid",
            borderWidth: "20px",
            borderColor: "#CAE8FF",
            borderRadius: "50px",
          }}
        >
          <Col xs={7} className="mb-4">
            <p
              className="d-flex justify-content-center"
              style={{
                color: "black",
                background: "#CAE8FF",
                fontSize: "36px",
                borderRadius: "20px",
              }}
            >
              เลือกอัตราทดเกียร์มอเตอร์ที่เหมาะสม
            </p>
          </Col>
          <Col xs={6}>
            <img src="allblue.png" width={"100%"} />
          </Col>
          <Col xs={6}>
            <Row>
              <Col xs={4}></Col>
              <Col xs={4} className="d-flex flex-column align-items-center">
                <div className="mb-3">
                  <p style={{ fontSize: "26px" }}>
                    <b>น้ำหนัก (Mass)</b>
                  </p>
                </div>
                <p
                  className="p-3 d-flex justify-content-center rounded w-100"
                  style={{
                    background: "#3A9BDC",
                    color: "black",
                    fontSize: "26px",
                  }}
                >
                  <b>{grum}</b>
                </p>
              </Col>
              <Col
                xs={4}
                className="d-flex flex-column justify-content-end pb-4"
              >
                <p style={{ fontSize: "26px" }}><b>g</b></p>
              </Col>
              <Col
                xs={4}
                className="d-flex flex-column align-items-center my-4"
              ></Col>
              <Col
                xs={4}
                className="d-flex flex-column align-items-center my-4"
              >
                <p style={{ fontSize: "26px" }}>
                  <b>คู่ที่ 1</b>
                </p>
              </Col>
              <Col
                xs={4}
                className="d-flex flex-column align-items-center my-4"
              >
                <p style={{ fontSize: "26px" }}>
                  <b>คู่ที่ 2</b>
                </p>
              </Col>
              <Col
                xs={4}
                className="d-flex flex-column align-items-center  justify-content-center my-3"
              >
                <p style={{ fontSize: "26px" }}>
                  <b>จำนวนเฟืองขับ</b>
                </p>
              </Col>
              <Col
                xs={4}
                className="d-flex flex-column align-items-center justify-content-center my-3"
              >
                <Form.Select
                  size="sm"
                  className="w-100 rounded"
                  style={{
                    backgroundColor: "#3A9BDC",
                    color: "black",
                    fontSize: "30px",
                    textAlign: "center",
                  }}
                  onChange={(e) => {
                    setValueOne(e.target.value);
                    // handleSend();
                    handleSendTwo(
                      e.target.value,
                      valueTwo,
                      valueThree,
                      valueFour
                    );
                  }}
                >
                  {gearRatio.map((el, index) => (
                    <option value={el.value} key={index}>
                      <b>{el.label}</b>
                    </option>
                  ))}
                </Form.Select>
              </Col>
              <Col
                xs={4}
                className="d-flex flex-column align-items-center  justify-content-center my-3"
              >
                <Form.Select
                  size="sm"
                  className="w-100 rounded"
                  style={{
                    backgroundColor: "#3A9BDC",
                    color: "black",
                    fontSize: "30px",
                    textAlign: "center",
                  }}
                  onChange={(e) => {
                    setValueThree(e.target.value);
                    // handleSend();
                    handleSendTwo(
                      valueOne,
                      valueTwo,
                      e.target.value,
                      valueFour
                    );
                  }}
                >
                  {gearRatioTwo.map((el, index) => (
                    <option value={el.value} key={index}>
                      <b>{el.label}</b>
                    </option>
                  ))}
                </Form.Select>
              </Col>
              <Col
                xs={4}
                className="d-flex flex-column align-items-center  justify-content-center my-3"
              >
                <p style={{ fontSize: "26px" }}>
                  <b>จำนวนเฟืองตาม</b>
                </p>
              </Col>
              <Col
                xs={4}
                className="d-flex flex-column align-items-center justify-content-center my-3"
              >
                <Form.Select
                  size="sm"
                  className="w-100 rounded"
                  style={{
                    backgroundColor: "#3A9BDC",
                    color: "black",
                    fontSize: "30px",
                    textAlign: "center",
                  }}
                  onChange={(e) => {
                    setValueTwo(e.target.value);
                    // handleSend();
                    handleSendTwo(
                      valueOne,
                      e.target.value,
                      valueThree,
                      valueFour
                    );
                  }}
                >
                  {gearRatioThree.map((el, index) => (
                    <option value={el.value} key={index}>
                      <b>{el.label}</b>
                    </option>
                  ))}
                </Form.Select>
              </Col>
              <Col
                xs={4}
                className="d-flex flex-column align-items-center  justify-content-center my-3"
              >
                <Form.Select
                  size="sm"
                  className="w-100 rounded"
                  style={{
                    backgroundColor: "#3A9BDC",
                    color: "black",
                    fontSize: "30px",
                    textAlign: "center",
                  }}
                  onChange={(e) => {
                    setValueFour(e.target.value);
                    // handleSend();
                    handleSendTwo(
                      valueOne,
                      valueTwo,
                      valueThree,
                      e.target.value
                    );
                  }}
                >
                  {gearRatioThree.map((el, index) => (
                    <option value={el.value} key={index}>
                      <b>{el.label}</b>
                    </option>
                  ))}
                </Form.Select>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <div className="w-75 d-flex flex-column align-items-center">
        <div className="d-flex w-75 justify-content-start">
          <p className="mt-5" style={{ fontSize: "26px" }}>
            <b>แรงบิดผ่านอัตราทดเกียร์ =</b>
          </p>
          <div className="d-flex align-items-center mx-3">
            {/* <p
              className="p-3 d-flex justify-content-center rounded"
              style={{ background: "#737373", color: "white" }}
            > */}
            <Form.Control
              placeholder="Username"
              aria-label="Username"
              aria-describedby="basic-addon1"
              style={{
                background: "white",
                border: "solid",
                borderWidth: "20px",
                borderColor: "#CAE8FF",
                borderRadius: "50px",
                textAlign: "center",
                fontSize: "26px",
              }}
              value={show != 0 && show.nm ? show.nm : "-"}
              disabled
            />
            {/* </p> */}
          </div>
          <p className="my-5" style={{ fontSize: "26px" }}>
            <b>N.m.</b>
          </p>
        </div>
        <div className="d-flex w-75 justify-content-start">
          <Dropdown>
            <Dropdown.Toggle variant="primary" id="dropdown-basic">
              สูตร
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item>
                สูตร แรงบิดผ่านอัตราทดเกียร์ = อัตราทดเกียร์ x แรงบิดมอเตอร์
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
      <div className="w-75 d-flex flex-column align-items-center">
        <div className="d-flex w-75 justify-content-start align-items-center">
          <p className="" style={{ fontSize: "26px" }}>
            <b>อัตราทดเกียร์ =</b>
          </p>
          <div className="d-flex align-items-center mx-3">
            <Form.Control
              aria-describedby="basic-addon1"
              style={{
                background: "white",
                border: "solid",
                borderWidth: "20px",
                borderColor: "#CAE8FF",
                borderRadius: "50px",
                textAlign: "center",
                fontSize: "26px",
              }}
              onChange={(e) => {
                setRating(e.target.value);
              }}
            />
          </div>
          <p className="my-5" style={{ fontSize: "16px" }}>
            {/* <b>N.m.</b> */}
          </p>
        </div>
        <div className="d-flex w-75 justify-content-start">
          <Dropdown>
            <Dropdown.Toggle variant="primary" id="dropdown-basic">
              สูตร
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item>สูตร</Dropdown.Item>
              <Dropdown.Item>
                อัตราทดเกียร์คู่ที่ 1 = จำนวนเฟืองตามที่ 1 / จำนวนเฟืองขับที่ 1
              </Dropdown.Item>
              <Dropdown.Item>
                อัตราทดเกียร์คู่ที่ 2 = จำนวนเฟืองตามที่ 2 / จำนวนเฟืองขับที่ 2
              </Dropdown.Item>
              <Dropdown.Item>
                อัตราทดเกียร์รวม = อัตราทดเกียร์คู่ 1 x อัตราทดเกียร์คู่ที่ 2
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
      <div className="w-75 d-flex flex-column align-items-center">
        <div className="d-flex w-75 justify-content-start align-items-center">
          <p className="" style={{ fontSize: "26px" }}>
            <b>ความเร็วรอบผ่านอัตราทดเกียร์ =</b>
          </p>
          <div className="d-flex align-items-center mx-3">
            <Form.Control
              aria-describedby="basic-addon1"
              style={{
                background: "white",
                border: "solid",
                borderWidth: "20px",
                borderColor: "#CAE8FF",
                borderRadius: "50px",
                textAlign: "center",
                fontSize: "26px",
              }}
              onChange={(e) => {
                setRpm(e.target.value);
              }}
            />
          </div>
          <p className="my-5" style={{ fontSize: "26px" }}>
            <b>RPM</b>
          </p>
        </div>
        <div className="d-flex w-75 justify-content-start">
          <Dropdown>
            <Dropdown.Toggle variant="primary" id="dropdown-basic">
              สูตร
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item>สูตร</Dropdown.Item>
              <Dropdown.Item>
                ความเร็วรอบผ่านอัตราทดเกียร์ (RPM) = ความเร็วรอบมอเตอร์ (RPM) /
                อัตราทดเกียร์
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
      <div className="w-75 d-flex flex-column align-items-center">
        <div className="d-flex w-75 justify-content-start align-items-center">
          <p className="" style={{ fontSize: "26px" }}>
            <b>กำลังไฟฟ้ามอเตอร์เมื่อต่ออัตราทดเกียร์ =</b>
          </p>
          <div className="d-flex align-items-center mx-3">
            <Form.Control
              aria-describedby="basic-addon1"
              style={{
                background: "white",
                border: "solid",
                borderWidth: "20px",
                borderColor: "#CAE8FF",
                borderRadius: "50px",
                textAlign: "center",
                fontSize: "26px",
              }}
              onChange={(e) => {
                setPower(e.target.value);
              }}
            />
          </div>
          <p className="my-5" style={{ fontSize: "26px" }}>
            <b>W</b>
          </p>
        </div>
        <div className="d-flex w-75 justify-content-start">
          <Dropdown>
            <Dropdown.Toggle variant="primary" id="dropdown-basic">
              สูตร
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item>สูตร</Dropdown.Item>
              <Dropdown.Item>
                กำลังไฟฟ้า = ( แรงบิดมอเตอร์เมื่อต่ออัตราทดเกียร์ x
                ควาเร็วรอบเมื่อต่ออัตราทดเกียร์ ) / 9.55
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
      <button
        className="btn my-3"
        onClick={() => {
          handleSend();
          setCheckValue(true);
        }}
        style={{ zIndex: "2", fontSize: "36px", background: "#FFDE59" }}
      >
        ทดสอบ
      </button>
      {checkValue && show != 0 ? (
        <>
          <div className="w-100 my-4 d-flex justify-content-center">
            <div
              className="w-50 p-3 d-flex flex-column justify-content-center align-items-center"
              style={{
                background: "white",
                border: "solid",
                borderWidth: "10px",
                borderColor: "#3A9BDC",
                borderRadius: "40px",
              }}
            >
              <p
                className="p-4 mb-5"
                style={{
                  background: "#5CB6F9",
                  borderRadius: "20px",
                  fontSize: "36px",
                }}
              >
                <b>รูปแบบเฟือง</b>
              </p>
              <img src={show.img} width={"500px"} />
            </div>
          </div>
          <div
            className="w-75 p-5 bold"
            style={{
              background: "#CAE8FF",
              fontSize: "36px",
              borderRadius: "30px",
            }}
          >
            <Row>
              <Col
                xs={6}
                className="d-flex justify-content-start align-items-center"
              >
                <p className="ms-5">ผลจากการคำนวณ</p>
              </Col>
              <Col xs={6}>
                <Row>
                  <Col
                    xs={12}
                    className="d-flex justify-content-center align-items-center"
                  >
                    <p>ผลจากการทดลอง</p>
                  </Col>
                  <Col
                    xs={6}
                    className="d-flex justify-content-center align-items-center"
                  >
                    <p>ไม่มีโหลด</p>
                  </Col>
                  <Col
                    xs={6}
                    className="d-flex justify-content-center align-items-center"
                  >
                    <p>มีโหลด</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div
            className="w-75 p-3 my-2"
            style={{
              background: "#CAE8FF",
              fontSize: "36px",
              borderRadius: "30px",
            }}
          >
            <Row>
              <Col
                xs={4}
                className="d-flex justify-content-center align-items-center"
              >
                <b>{rpm}</b>
              </Col>
              <Col
                xs={2}
                className="d-flex justify-content-center align-items-center"
              >
                <p>
                  <b>ความเร็ว (RPM)</b>
                </p>
              </Col>
              <Col
                xs={6}
                className="d-flex justify-content-center align-items-center"
              >
                <Col
                  xs={6}
                  className="d-flex justify-content-center align-items-center"
                >
                  <b>{show.donLoad.rpm}</b>
                </Col>
                <Col
                  xs={6}
                  className="d-flex justify-content-center align-items-center"
                >
                  <b>{show.haveLoad.rpm}</b>
                </Col>
              </Col>
            </Row>
          </div>
          <div
            className="w-75 p-3 my-2"
            style={{
              background: "#CAE8FF",
              fontSize: "36px",
              borderRadius: "30px",
            }}
          >
            <Row>
              <Col
                xs={4}
                className="d-flex justify-content-center align-items-center"
              >
                <b>{powerm}</b>
              </Col>
              <Col
                xs={2}
                className="d-flex justify-content-center align-items-center"
              >
                <p>
                  <b>กำลังไฟฟ้า (W)</b>
                </p>
              </Col>
              <Col
                xs={6}
                className="d-flex justify-content-center align-items-center"
              >
                <Col
                  xs={6}
                  className="d-flex justify-content-center align-items-center"
                >
                  <b>{(show.donLoad.a * show.donLoad.v).toFixed(2)}</b>
                </Col>
                <Col
                  xs={6}
                  className="d-flex justify-content-center align-items-center"
                >
                  <b>{(show.haveLoad.a * show.haveLoad.v).toFixed(2)}</b>
                </Col>
              </Col>
            </Row>
          </div>
          <div
            className="w-75 p-3 my-2"
            style={{
              background: "#CAE8FF",
              fontSize: "36px",
              borderRadius: "30px",
            }}
          >
            <Row>
              <Col
                xs={4}
                className="d-flex justify-content-center align-items-center"
              >
                <b>{show.nm}</b>
              </Col>
              <Col
                xs={2}
                className="d-flex justify-content-center align-items-center"
              >
                <p>
                  <b>แรงบิด (N.M.)</b>
                </p>
              </Col>
              <Col
                xs={6}
                className="d-flex justify-content-center align-items-center"
              >
                <Col
                  xs={6}
                  className="d-flex justify-content-center align-items-center"
                >
                  <b>
                    {(
                      (9.55 * (show.donLoad.a * show.donLoad.v)) /
                      show.donLoad.rpm
                    ).toFixed(2)}
                  </b>
                </Col>
                <Col
                  xs={6}
                  className="d-flex justify-content-center align-items-center"
                >
                  <b>
                    {(
                      (9.55 * (show.haveLoad.a * show.haveLoad.v)) /
                      show.haveLoad.rpm
                    ).toFixed(2)}
                  </b>
                </Col>
              </Col>
            </Row>
          </div>
          <div
            className="h-75 d-flex justify-content-center"
            style={{ zIndex: "2", width: "90%" }}
          >
            <Row
              className="p-5 rounded d-flex justify-content-center"
              style={{ width: "90%" }}
            >
              <Col
                xs={7}
                className="d-flex flex-column p-4 rounded"
                style={{
                  background: "#CAE8FF",
                  fontSize: "26px",
                  color: "#233DFF",
                }}
              >
                <div className="d-flex">
                  <p>ความเหมาะสมการใช้งาน : </p>
                  <p style={{ color: "green" }}>&nbsp;{show.same}</p>
                </div>
                <p>
                  {valueThree == 0
                    ? "คำแนะนำ : สามารถทำการต่อเฟืองชุดที่ 2 เพื่อเพิ่มแรงบิดได้"
                    : null}
                </p>
              </Col>
              <Col xs={5} className="d-flex justify-content-end">
                <div
                  className="w-75 d-flex justify-content-center align-items-center p-3 rounded"
                  style={{
                    backgroundColor: "#F8D4FF",
                    fontSize: "26px",
                    cursor: "pointer",
                    borderRadius: "30px",
                  }}
                  onClick={() => {
                    window.location.href =
                      "https://demo.thingsboard.io/dashboard/8f468560-c24c-11ed-b62c-7d8052ad39cf?publicId=f35f17e0-a87c-11ed-b62c-7d8052ad39cf";
                  }}
                >
                  ทดลองด้วยตัวคุณเอง
                </div>
              </Col>
            </Row>
          </div>
        </>
      ) : null}
    </div>
  );
}

export default page4;
