import React, { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import cal10 from "../utiles/cal_10";
import cal20 from "../utiles/cal_20";
import cal30 from "../utiles/cal_30";
import cal40 from "../utiles/cal_40";
import cal50 from "../utiles/cal_50";

function page5({ data, grum }) {
  const [show, setShow] = useState(0);
  useEffect(() => {
    if (data[3] == 10) {
      setShow(cal10(data, grum));
    } else if (data[3] == 20) {
      setShow(cal20(data, grum));
    } else if (data[3] == 30) {
      setShow(cal30(data, grum));
    } else if (data[3] == 40) {
      setShow(cal40(data, grum));
    } else if (data[3] == 50) {
      setShow(cal50(data, grum));
    }
  }, [data, grum]);
  return (
    <div className="page d-flex flex-column justify-content-start align-items-center" style={{zIndex: "2"}}>
      <div className="w-75" style={{zIndex: "2"}}>
        <p
          style={{
            color: "#0BA1DD",
            fontSize: "37.6px",
            width: "fit-content"
          }}
          className="border border-primary rounded p-3"
        >
          อัตราทดเกียร์
        </p>
      </div>
      <div className="w-75 my-5" style={{ color: "white", zIndex: "2" }}>
        <Row>
          <Col xs={2} className="d-flex justify-content-center">
            <div
              className="w-75 d-flex justify-content-center p-3 rounded"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
              }}
            >
              <p>{show != 0 ? show.ratio : ":"}</p>
            </div>
          </Col>
          <Col xs={2} className="d-flex justify-content-center">
            <div
              className="w-100 d-flex justify-content-center p-3 rounded"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
              }}
            >
              <p>แรงดัน (V)</p>
            </div>
          </Col>
          <Col xs={2} className="d-flex justify-content-center">
            <div
              className="w-100 d-flex justify-content-center p-3 rounded"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
              }}
            >
              <p>กระแส (A)</p>
            </div>
          </Col>
          <Col xs={2} className="d-flex justify-content-center">
            <div
              className="w-100 d-flex justify-content-center p-3 rounded"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
              }}
            >
              <p>ค.เร็วรอบ (RPM)</p>
            </div>
          </Col>
          <Col xs={2} className="d-flex justify-content-center">
            <div
              className="w-100 d-flex justify-content-center p-3 rounded"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
              }}
            >
              <p>กำลังไฟฟ้า (W)</p>
            </div>
          </Col>
          <Col xs={2} className="d-flex justify-content-center">
            <div
              className="w-100 d-flex justify-content-center p-3 rounded"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
              }}
            >
              <p>แรงบิด (N.m)</p>
            </div>
          </Col>
          <Col xs={2} className="mt-5 p-0">
            <div
              className="w-100 d-flex justify-content-center p-3"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>ไม่มีภาระโหลด</p>
            </div>
          </Col>
          <Col xs={2} className="mt-5 p-0" style={{background: 'red'}}>
            <div
              className="w-100 h-100 d-flex justify-content-center p-3 m-0"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>{show != 0 ? show.donLoad.v : ""}</p>
            </div>
          </Col>
          <Col xs={2} className="mt-5 p-0" style={{background: 'red'}}>
            <div
              className="w-100 h-100 d-flex justify-content-center p-3 m-0"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>{show != 0 ? show.donLoad.a : ""}</p>
            </div>
          </Col>
          <Col xs={2} className="mt-5 p-0" style={{background: 'red'}}>
            <div
              className="w-100 h-100 d-flex justify-content-center p-3 m-0"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>{show != 0 ? show.donLoad.rpm : ""}</p>
            </div>
          </Col>
          <Col xs={2} className="mt-5 p-0" style={{background: 'red'}}>
            <div
              className="w-100 h-100 d-flex justify-content-center p-3 m-0"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>{show != 0 ? (show.donLoad.a * show.donLoad.v).toFixed(2) : ""}</p>
            </div>
          </Col>
          <Col xs={2} className="mt-5 p-0" style={{background: 'red'}}>
            <div
              className="w-100 h-100 d-flex justify-content-center p-3 m-0"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderRightWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>{show != 0 ? (9.55 * (show.donLoad.a * show.donLoad.v) / show.donLoad.rpm).toFixed(2) : ""}</p>
            </div>
          </Col>
          <Col xs={2} className="p-0">
            <div
              className="w-100 d-flex justify-content-center p-3"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderBottomWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>มีภาระโหลด</p>
            </div>
          </Col>
          <Col xs={2} className="p-0" style={{background: 'red'}}>
            <div
              className="w-100 h-100 d-flex justify-content-center p-3 m-0"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderBottomWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>{show != 0 ? show.haveLoad.v : ""}</p>
            </div>
          </Col>
          <Col xs={2} className="p-0" style={{background: 'red'}}>
            <div
              className="w-100 h-100 d-flex justify-content-center p-3 m-0"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderBottomWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>{show != 0 ? show.haveLoad.a : ""}</p>
            </div>
          </Col>
          <Col xs={2} className="p-0" style={{background: 'red'}}>
            <div
              className="w-100 h-100 d-flex justify-content-center p-3 m-0"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderBottomWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>{show != 0 ? show.haveLoad.rpm : ""}</p>
            </div>
          </Col>
          <Col xs={2} className="p-0" style={{background: 'red'}}>
            <div
              className="w-100 h-100 d-flex justify-content-center p-3 m-0"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderBottomWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>{show != 0 ? (show.haveLoad.a * show.haveLoad.v).toFixed(2) : ""}</p>
            </div>
          </Col>
          <Col xs={2} className="p-0" style={{background: 'red'}}>
            <div
              className="w-100 h-100 d-flex justify-content-center p-3 m-0"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                border: "solid",
                borderWidth: "3px",
                borderTopWidth: "0px",
                borderLeftWidth: "0px",
                borderRightWidth: "0px",
                borderBottomWidth: "0px",
                borderStyle: "dotted"
              }}
            >
              <p>{show != 0 ? (9.55 * (show.haveLoad.a * show.haveLoad.v) / show.haveLoad.rpm).toFixed(2) : ""}</p>
            </div>
          </Col>
          <Col xs={5} className="d-flex mt-5 justify-content-start">
            <div
              className="w-75 d-flex justify-content-center p-3 rounded"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
              }}
            >
              <p>ความเหมาะสมการใช้งาน: {show != 0 ? show.same : ":"}</p>
            </div>
          </Col>
          <Col xs={7} className="d-flex mt-5 justify-content-end">
            <div
              className="w-50 d-flex justify-content-center p-3 rounded"
              style={{
                backgroundColor: "#0BA1DD",
                fontSize: "20px",
                cursor: "pointer",
              }}
              onClick={()=>{
                window.location.href = "https://demo.thingsboard.io/dashboard/8f468560-c24c-11ed-b62c-7d8052ad39cf?publicId=f35f17e0-a87c-11ed-b62c-7d8052ad39cf"
              }}
            >
              <p>ลองด้วยตัวคุณเอง</p>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default page5;
