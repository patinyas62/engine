import React from "react";

function page6() {
  return (
    <div className="page" style={{ zIndex: "2" }}>
      <div
        className="page d-flex flex-column justify-content-start align-items-center"
        style={{ zIndex: "2" }}
      >
        <div className="w-75" style={{ zIndex: "2" }}>
          <p
            style={{
              color: "#0BA1DD",
              fontSize: "37.6px",
              width: "fit-content",
            }}
            className="border border-primary rounded p-3"
          >
            สูตรและการคำนวณ
          </p>
        </div>
        <div
          className="w-75 d-flex justify-content-center"
          style={{ zIndex: "2" }}
        >
          <div
            className="w-100 rounded mt-4 ms-5 d-flex flex-column justify-content-center"
            style={{
              fontSize: "30px",
              fontWeight: "bold",
            }}
          >
            <p>
              แรงบิด(N.m) &nbsp;&nbsp;&nbsp; = &nbsp;&nbsp;&nbsp; 9.55
              &nbsp;&nbsp;&nbsp; x &nbsp;&nbsp;&nbsp; กำลังไฟฟ้า(W)
              &nbsp;&nbsp;&nbsp; / &nbsp;&nbsp;&nbsp; ความเร็วรอบ (RPM)
            </p>
            <p>
              กำลังไฟฟ้า(W) &nbsp;&nbsp;&nbsp; = &nbsp;&nbsp;&nbsp; แรงดัน(V)
              &nbsp;&nbsp;&nbsp; x &nbsp;&nbsp;&nbsp;แอมป์(A)
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default page6;
